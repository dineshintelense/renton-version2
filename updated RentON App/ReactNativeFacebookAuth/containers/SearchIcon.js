import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  Platform,
  FlatList,
  ActivityIndicator
} from "react-native";
import { SearchBar } from "react-native-elements";
import { withNavigation } from "react-navigation";
import { products } from "../Data";
const API_ENDPOINT = `https://randomuser.me/api/?`;
function SearchIcon() {
  const [query, setQuery] = useState("");
  const [fullData, setFullData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [products, setData] = useState([]);
  const [error, setError] = useState(null);

  // state variables defined

  useEffect(() => {
    setIsLoading(true);

    fetch(API_ENDPOINT)
      .then(response => response.json())
      .then(response => {
        setData(response.results);

        // ADD THIS
        setFullData(response.results);

        setIsLoading(false);
      })
      .catch(err => {
        setIsLoading(false);
        setError(err);
      });
  }, []);

  const handleSearch = text => {
    const formattedQuery = text.toLowerCase();
    const filteredData = filter(fullData, user => {
      return contains(user, formattedQuery);
    });
    setData(filteredData);
    setQuery(text);
  };

  const contains = ({ name }, query) => {
    if (name.includes(query)) return true;
  };

  return (
    <View style={styles.container}>
      <FlatList
        products={products}
        keyExtractor={item => item.pid}
        renderItem={({ item }) => (
          <View style={styles.listItem}>
            <Text style={styles.listItemText}>{item.name}</Text>
          </View>
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#f8f8f8",
    alignItems: "center"
  },
  text: {
    fontSize: 20,
    color: "#101010",
    marginTop: 60,
    fontWeight: "700"
  },
  listItem: {
    marginTop: 10,
    padding: 20,
    alignItems: "center",
    backgroundColor: "#fff",
    width: "100%"
  },
  listItemText: {
    fontSize: 18
  }
});
export default withNavigation(SearchIcon);
