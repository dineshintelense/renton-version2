import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Button } from "react-native";
import { FontAwesome } from "@expo/vector-icons";
import { Card } from "react-native-elements";
import StarRating from "../views/StarRating";
import { AntDesign } from "@expo/vector-icons";
import Like from "../views/like";
import { connect } from "react-redux";

class ProductDetailsScreen extends React.Component {
  render() {
    const { navigation } = this.props;
    const img = navigation.getParam("img");
    const price = navigation.getParam("price");
    const name = navigation.getParam("name");

    return (
      <View style={styles.container}>
        <br></br>
        <Card
          containerStyle={{
            elevation: 0,
            borderColor: "Purple",
            borderRadius: 30
          }}
        >
          <Text style={styles.name1} h2>
            <AntDesign
              name="left"
              size={24}
              color="Purple"
              onPress={() => this.props.navigation.navigate("Home")}
            />
            {"          "}
            Garden & Lawn
            {"          "}
            <Like />
          </Text>
          <br></br>
          <br></br>
          <Card
            image={{ uri: img }}
            containerStyle={{ elevation: 0, borderColor: "transparent" }}
          />
          <br></br>
          <br></br>
          <Text style={styles.name} h2>
            {name}
          </Text>
          <Text style={styles.price} h2>
            {price}
          </Text>
          <Text style={styles.name}>
            <StarRating />
          </Text>
        </Card>
        <br></br>

        <TouchableOpacity
          style={styles.button}
          onPress={() =>
            this.props.navigation.navigate(
              "shoppingcart",
              name,

              price,

              img
            )
          }
        >
          <Text style={styles.buttonText}>ADD TO CART</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonroundLeft}>
          <AntDesign
            name="arrowleft"
            size={24}
            color="white"
            justifyContent="center"
            textAlign="center"
            alignItems="center"
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonroundRight}>
          <AntDesign
            name="arrowright"
            size={24}
            color="white"
            justifyContent="center"
            textAlign="center"
            alignItems="center"
          />
        </TouchableOpacity>

        <br></br>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  name: {
    color: "purple",
    fontSize: 20,
    alignItems: "center",
    textAlign: "center"
  },
  name1: {
    color: "purple",
    fontSize: 20,
    alignItems: "center",
    textAlign: "center",

    justifyContent: "space-around"
  },
  buttonroundLeft: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: "purple",
    borderRadius: 400,
    justifyContent: "space-round",
    position: "absolute",
    height: 50,
    width: 50,
    top: 10,
    marginTop: 500,
    marginLeft: 230
  },
  buttonroundRight: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    backgroundColor: "purple",
    borderRadius: 400,
    justifyContent: "space-round",
    position: "absolute",
    height: 50,
    width: 50,
    top: 10,
    marginTop: 500,
    marginLeft: 290,
    marginRight: 20
  },
  button: {
    paddingVertical: 15,
    paddingHorizontal: 15,
    marginTop: 500,
    marginLeft: 20,
    width: 150,
    backgroundColor: "purple",
    justifyContent: "space-round",
    alignItems: "space-round",
    textAlign: "center",
    position: "absolute",
    height: 50,
    top: 10,
    borderRadius: 30
  },
  buttonText: {
    color: "white",
    fontWeight: "bold",
    textTransform: "uppercase",
    fontSize: 15,
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center"
  },

  price: {
    fontWeight: "bold",
    marginBottom: 10,
    alignItems: "center",
    textAlign: "center",
    fontSize: 30,
    color: "purple"
  },
  description: {
    fontSize: 10,
    color: "#c1c4cd"
  },
  container: {
    justifyContent: "top",
    alignItems: "top",

    flex: 1,
    backgroundColor: "white"
  },
  details: {
    justifyContent: "top",
    alignItems: "top",
    borderRadius: 20,
    flex: 1,
    backgroundColor: "white",
    height: 500
  }
});
export default ProductDetailsScreen;
